import * as React from 'react';
import { subscribe } from 'react-contextual';

interface IProps {
    count: number
    increment: () => void
}

const Home: React.SFC<IProps> = (props: IProps) => {
  return (
      <div>
          <span>{props.count}</span>
          <button onClick={props.increment}>Increment</button>
      </div>
  );
};

const HomeSubscriber = subscribe()(Home);

export { Home, HomeSubscriber };
