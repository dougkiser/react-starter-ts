import * as React from 'react';
import * as Enzyme from 'enzyme';
import {Home} from '../index';

test('component renders without crashing', () => {
    Enzyme.shallow(<Home count={0} increment={()=>{}}/>);
});

test('component displays count', () => {
    const wrapper = Enzyme.mount(<Home count={0} increment={()=>{}}/>);
    expect(wrapper.find('span').text()).toBe('0');
});

test('component renders without crashing', () => {
    const increment = jest.fn();
    const wrapper = Enzyme.mount(<Home count={0} increment={increment} />);
    wrapper.find('button').simulate('click');
    expect(increment.mock.calls.length).toBe(1);
});