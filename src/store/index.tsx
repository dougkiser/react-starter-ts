import { buildInformationState, buildActions } from './build-information/index';

export default ({
  ...buildInformationState,
  ...buildActions,
});
