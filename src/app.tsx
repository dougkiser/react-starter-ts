import * as React from 'react';
import { Provider } from 'react-contextual';
import { BrowserRouter as Router } from 'react-router-dom';
import store from './store';
import Routes from './routes';
import './app.css';

const App: React.SFC = () => {
    return (
        <div className='app'>
            <Provider {...store}>
                <Router>
                    <Routes />
                </Router>
            </Provider>
        </div>
    );
};

export default App;