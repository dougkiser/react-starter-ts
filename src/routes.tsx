import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { HomeSubscriber } from './containers/home';

const Routes: React.SFC = () => (
    <Switch>
        <Route exact path='/' component={HomeSubscriber} />
        <Route component={HomeSubscriber} />
    </Switch>
);

export default Routes;